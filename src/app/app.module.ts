import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule, Routes} from '@angular/router';
import {AngularFireModule} from 'angularfire2';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { DemoComponent } from './demo/demo.component';
import { PostsComponent } from './posts/posts.component';
import { PostsService } from './posts/posts.service';
import { PostComponent } from './post/post.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { UserComponent } from './user/user.component';
import { UserFormComponent } from './user-form/user-form.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PostFormComponent } from './post-form/post-form.component';

const appRoutes:Routes =[
  {path:'users' ,component:UsersComponent},
  {path:'posts' ,component:PostsComponent},
  {path:'' ,component:UsersComponent},
  {path:'**' ,component:PageNotFoundComponent},
]

export const firebaseConfig = {
    apiKey: "AIzaSyDDZZr50UDeysMAdmiPmahITivS5uOBZCs",
    authDomain: "davidan-2a8da.firebaseapp.com",
    databaseURL: "https://davidan-2a8da.firebaseio.com",
    storageBucket: "davidan-2a8da.appspot.com",
    messagingSenderId: "834146137770"
}


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    DemoComponent,
    PostsComponent,
    PostComponent,
    SpinnerComponent,
    UserComponent,
    UserFormComponent,
    PageNotFoundComponent,
    PostFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [PostsService],
  bootstrap: [AppComponent]
})
export class AppModule { }

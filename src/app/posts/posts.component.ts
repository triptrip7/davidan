import { Component, OnInit } from '@angular/core';
import {PostsService} from './posts.service';

@Component({
  selector: 'jce-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  isLoading:Boolean = true;
  posts;

  currentPost;

  select(post){
    this.currentPost = post;
  }

  constructor(private _postsService: PostsService) { }

  deletePost(post){
    this._postsService.deletePost(post);
  }

  updateUser(post){
    this._postsService.updatePost(post);
   }  

   addPost(post){
    this._postsService.addPost(post);
  }

  ngOnInit() {
    this._postsService.getPosts().subscribe(postsData =>
    {this.posts = postsData;
     this.isLoading = false});
  }

} 

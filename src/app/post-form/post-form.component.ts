import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {NgForm} from '@angular/forms';
import {Post} from '../post/post'

@Component({
  selector: 'jce-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {
   @Output() postAddedEvent = new EventEmitter<Post>();
   post:Post = {
    title: '',
    content: '',
    author: ''
  };
  //post:Post = {title:'',content:'', author:''};

  constructor() { }

  onSubmit(form:NgForm){
    console.log(form);
    this.postAddedEvent.emit(this.post);
    this.post = {
      title: '',
      content: '',
      author: ''
    }
  }

  ngOnInit() {
  }

} 

import { DavidanPage } from './app.po';

describe('davidan App', function() {
  let page: DavidanPage;

  beforeEach(() => {
    page = new DavidanPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
